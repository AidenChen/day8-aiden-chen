package com.example.demo.service;

import com.example.demo.exception.EmployeeCreatedException;
import com.example.demo.model.Employee;
import com.example.demo.repository.EmployeeRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.ValueSource;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class EmployeeServiceTest {
    private final EmployeeRepository employeeRepository = mock(EmployeeRepository.class);
    private final EmployeeService employeeService = new EmployeeService(employeeRepository);



    @ParameterizedTest
    @ValueSource(ints = {18, 65})
    void should_return_created_employee_when_create_employee_given_employee_age_is_between_18_to_65(Integer age) {
        Employee employee = new Employee(null, "Lily", age, "Female", 20000);
        when(employeeRepository.insert(employee)).thenReturn(new Employee(1L, "Lily", age, "Female", 20000));
        Employee employeeResponse = employeeService.create(employee);


        assertNotNull(employeeResponse);
        assertEquals(1L, employeeResponse.getId());
        assertEquals("Lily", employeeResponse.getName());
        assertEquals(age, employeeResponse.getAge());
        assertEquals("Female", employeeResponse.getGender());
        assertEquals(20000, employeeResponse.getSalary());
    }


    @ParameterizedTest
    @ValueSource(ints = {17, 66})
    void should_throw_exception_when_create_employee_given_employee_age_is_not_between_18_and_65(Integer age) {
        Employee employee = new Employee(null, "Lily", age, "Female", 20000);

        EmployeeCreatedException exception = assertThrows(EmployeeCreatedException.class, () -> employeeService.create(employee));
        assertEquals("Employee must be 18~65 years old", exception.getMessage());
    }

    @ParameterizedTest(name = "{index} employee age is {0} and salary is {1}")
    @CsvSource({"30,20000", "29,19999"})
    void should_return_created_employee_when_create_employee_given_(Integer age, Integer salary) {
        Employee employee = new Employee(null, "Lily", age, "Female", salary);
        when(employeeRepository.insert(employee)).thenReturn(new Employee(1L, "Lily", age, "Female", salary));

        Employee employeeResponse = employeeService.create(employee);

        assertNotNull(employeeResponse);
        assertEquals(1L, employeeResponse.getId());
        assertEquals("Lily", employeeResponse.getName());
        assertEquals(age, employeeResponse.getAge());
        assertEquals("Female", employeeResponse.getGender());
        assertEquals(salary, employeeResponse.getSalary());
    }

    @Test
    void should_throw_exception_when_create_employee_given_age_is_30_and_salary_is_19999() {
        Employee employee = new Employee(null, "Lily", 30, "Female", 19999);

        EmployeeCreatedException exception = assertThrows(EmployeeCreatedException.class, () -> employeeService.create(employee));
        assertEquals("Employees over 30 years of age (inclusive) with a salary below 20000 cannot be created", exception.getMessage());
    }

    @Test
    void should_employee_status_is_true_when_create_employee_given_employee() {
        Employee employee = new Employee(null, "Lily", 18, "Female", 19999);
        when(employeeRepository.insert(employee)).thenReturn(new Employee(1L, "Lily", 18, "Female", 19999, null, false));
        Employee employeeResponse = employeeService.create(employee);

        assertNotNull(employeeResponse);
        assertEquals(1L, employeeResponse.getId());
        assertEquals("Lily", employeeResponse.getName());
        assertEquals(18, employeeResponse.getAge());
        assertEquals("Female", employeeResponse.getGender());
        assertEquals(19999, employeeResponse.getSalary());
        assertEquals(true, employeeResponse.getStatus());

    }

    @Test
    void should_employee_status_is_false_when_delete_employee_given_employee_id() {
        when(employeeRepository.delete(1L)).thenReturn(new Employee(1L, "Lily", 18, "Female", 19999, null, false));
        Employee employee = employeeService.delete(1L);
        assertNotNull(employee);
        assertEquals(1L, employee.getId());
        assertEquals("Lily", employee.getName());
        assertEquals(18, employee.getAge());
        assertEquals("Female", employee.getGender());
        assertEquals(19999, employee.getSalary());
        assertEquals(false, employee.getStatus());
    }

    @Test
    void should_reject_to_update_when_update_employee_given_employee_in_the_company() {
        Employee employee = new Employee(null, "Lily", 18, "Female", 19999, 1L, true);
        when(employeeRepository.update(1L, employee)).thenReturn(new Employee(1L, "Lily", 18, "Female", 19999, 1L, true));
        Employee updateEmploy = employeeService.update(1L, employee);
        assertNotNull(employee);
        assertEquals(1L, updateEmploy.getId());
        assertEquals("Lily", updateEmploy.getName());
        assertEquals(18, updateEmploy.getAge());
        assertEquals("Female", updateEmploy.getGender());
        assertEquals(19999, updateEmploy.getSalary());
        assertEquals(1L, updateEmploy.getCompanyId());
        assertEquals(true, updateEmploy.getStatus());
    }

    @Test
    void should_reject_to_update_when_update_employee_given_employee_left_the_company() {
        Employee employee = new Employee(null, "Lily", 18, "Female", 19999, null, false);
        when(employeeRepository.update(1L, employee)).thenReturn(new Employee(1L, "Lily", 20, "Female", 2000, null, false));
        Employee updateEmploy = employeeService.update(1L, employee);
        assertNotNull(employee);
        assertEquals(1L, updateEmploy.getId());
        assertEquals("Lily", updateEmploy.getName());
        assertEquals(20, updateEmploy.getAge());
        assertEquals("Female", updateEmploy.getGender());
        assertEquals(2000, updateEmploy.getSalary());
        assertNull(updateEmploy.getCompanyId());
        assertEquals(false, updateEmploy.getStatus());
    }


    @Test
    void should_employee_when_get_employee_given_id() {
        when(employeeRepository.findById(1L)).thenReturn(new Employee(1L, "Lily", 20, "Female", 2000));
        Employee employee = employeeService.findById(1L);
        assertNotNull(employee);
        assertEquals(1L, employee.getId());
        assertEquals("Lily", employee.getName());
        assertEquals(20, employee.getAge());
        assertEquals("Female", employee.getGender());
        assertEquals(2000, employee.getSalary());
    }


    @Test
    void should_employees_when_get_all_employee_given_nothing() {
        List<Employee> employees = new ArrayList<>();
        employees.add(new Employee(1L, "Lily", 20, "Female", 2000));
        employees.add(new Employee(2L, "Aiden", 18, "Female", 1254));
        when(employeeRepository.findAll()).thenReturn(employees);
        List<Employee> employeesResponse = employeeService.findAll();
        assertNotNull(employeesResponse);
        assertEquals(employeesResponse.get(0), employees.get(0));
        assertEquals(employeesResponse.get(1), employees.get(1));
    }

    @Test
    void should_employees_when_get_employee_given_gender() {
        List<Employee> femaleEmployees = new ArrayList<>();
        femaleEmployees.add(new Employee(1L, "Lily", 20, "Female", 2000));
        femaleEmployees.add(new Employee(2L, "Aiden", 18, "Female", 1254));
        when(employeeRepository.findByGender("Female")).thenReturn(femaleEmployees);
        List<Employee> employeesResponse = employeeService.findByGender("Female");
        assertNotNull(employeesResponse);
        assertEquals(employeesResponse.get(0), femaleEmployees.get(0));
        assertEquals(employeesResponse.get(1), femaleEmployees.get(1));
    }

    @Test
    void should_employees_when_get_employee_given_page_and_size() {
        List<Employee> employees = new ArrayList<>();
        employees.add(new Employee(1L, "Lily", 20, "Female", 2000));
        employees.add(new Employee(2L, "Aiden", 18, "Female", 1254));
        employees.add(new Employee(2L, "Aiden", 18, "Female", 1254));
        employees.add(new Employee(2L, "Aiden", 18, "Female", 1254));
        employees.add(new Employee(2L, "Aiden", 18, "Female", 1254));
        when(employeeRepository.findAll()).thenReturn(employees);
        List<Employee> employeesResponse = employeeService.findByPage(1,2);
        assertNotNull(employeesResponse);
        assertEquals(employeesResponse.size(), 2);
        assertEquals(employeesResponse.get(0), employees.get(0));
        assertEquals(employeesResponse.get(1), employees.get(1));
    }





}
