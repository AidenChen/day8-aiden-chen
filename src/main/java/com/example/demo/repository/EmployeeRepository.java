package com.example.demo.repository;


import com.example.demo.exception.NotFoundException;
import com.example.demo.model.Employee;
import org.springframework.stereotype.Repository;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Repository
public class EmployeeRepository {
    private final List<Employee> employees = new ArrayList<>();

    public EmployeeRepository() {
        this.employees.add(new Employee(1L, "Lily1", 20, "Female", 8000, 1L));
        this.employees.add(new Employee(2L, "Lily2", 20, "Female", 8000, 1L));
        this.employees.add(new Employee(3L, "Lily3", 20, "Female", 8000, 1L));
        this.employees.add(new Employee(4L, "Lily4", 20, "Female", 8000, 2L));
        this.employees.add(new Employee(5L, "Lily5", 20, "Female", 8000, 2L));
        this.employees.add(new Employee(6L, "Lily6", 20, "Female", 8000, 2L));
    }

    public List<Employee> findAll() {
        return employees;
    }


    public List<Employee> findByGender(String gender) {
        return employees.stream()
                .filter(employee -> employee.getGender().equals(gender))
                .collect(Collectors.toList());
    }

    public List<Employee> findByCompanyId(Long companyId) {
        return employees.stream()
                .filter(employee -> Objects.equals(employee.getCompanyId(), companyId))
                .collect(Collectors.toList());
    }

    public Employee insert(Employee newEmployee) {
        employees.add(newEmployee);
        return newEmployee;
    }

    public Employee update(Long id, Employee employee) {
        Employee employeeToUpdate = findById(id);
        employeeToUpdate.merge(employee);
        return employeeToUpdate;
    }

    public Employee delete(Long id) {
        Employee employee = findById(id);
        employees.remove(employee);
        return employee;
    }

    public void clearAll() {
        employees.clear();
    }


    public Employee findById(Long id) {
        return employees.stream()
                .filter(employee -> Objects.equals(employee.getId(), id))
                .findFirst()
                .orElseThrow(NotFoundException::new);
    }
}

