package com.example.demo.repository;

import com.example.demo.model.Company;
import org.springframework.stereotype.Repository;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Repository
public class CompanyRepository {
    private static final List<Company> companies = new ArrayList<>();

    public CompanyRepository() {
        companies.add(new Company(1L, "spring"));
        companies.add(new Company(2L, "boot"));
    }

    public List<Company> getCompanies() {
        return companies;
    }

    public Company findById(Long id) {
        return getCompanies().stream()
                .filter(company -> company.getId().equals(id))
                .findFirst()
                .orElse(null);
    }


    public Company addCompany(Company company) {
        companies.add(company);
        return company;
    }

    public void delete(Long id) {
        getCompanies().removeIf(company -> Objects.equals(company.getId(), id));
    }

    public Company update(Company companyStored, Company company) {
        if (company.getName() != null) {
            companyStored.setName(company.getName());
        }
        return companyStored;
    }

    private Long generateNewId() {
        return companies.stream()
                .mapToLong(Company::getId)
                .max()
                .orElse(0L) + 1;
    }
}

