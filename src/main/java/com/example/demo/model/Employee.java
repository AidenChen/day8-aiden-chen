package com.example.demo.model;

import java.util.Objects;

public class Employee {
    private Long id;
    private String name;
    private Integer age;
    private String gender;
    private Integer salary;
    private Long companyId;

    private Boolean status;

    public Employee() {
    }

    public Employee(Long id, String name, Integer age, String gender, Integer salary) {
        this.id = id;
        this.name = name;
        this.age = age;
        this.gender = gender;
        this.salary = salary;
    }

    public Employee(Long id, String name, Integer age, String gender, Integer salary, Long companyId) {
        this(id, name, age, gender, salary);
        this.companyId = companyId;
    }

    public Employee(Long id, String name, Integer age, String gender, Integer salary, Long companyId, Boolean status) {
        this(id, name, age, gender, salary, companyId);
        this.status = status;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public Integer getSalary() {
        return salary;
    }

    public void setSalary(Integer salary) {
        this.salary = salary;
    }

    public Long getCompanyId() {
        return companyId;
    }

    public void setCompanyId(Long companyId) {
        this.companyId = companyId;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public void merge(Employee employee) {
        this.salary = employee.getSalary();
        this.age = employee.getAge();
        this.companyId = employee.getCompanyId();
    }

    public boolean isAgeValid() {
        return getAge() >= 18 && getAge() <= 65;
    }

    public boolean isSalaryValid() {
       return  !(getAge() >= 30 && getSalary() < 20000);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Employee employee = (Employee) o;
        return Objects.equals(id, employee.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
