package com.example.demo.service;

import com.example.demo.model.Company;
import com.example.demo.repository.CompanyRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class CompanyService {

    private final CompanyRepository companyRepository;

    public CompanyService(CompanyRepository companyRepository) {
        this.companyRepository = companyRepository;
    }

    public List<Company> getCompanies() {
        return companyRepository.getCompanies();
    }

    public Company findById(Long id) {
        return companyRepository.findById(id);
    }

    public List<Company> findByPage(Integer page, Integer size) {
        return getCompanies().stream()
                .skip((long) (page - 1) * size)
                .limit(size)
                .collect(Collectors.toList());
    }

    public Company addCompany(Company company) {
        company.setId(generateNewId());
        return companyRepository.addCompany(company);
    }

    public Company update(Long id, Company company) {
        return getCompanies().stream()
                .filter(storedCompany -> storedCompany.getId().equals(id))
                .findFirst()
                .map(storedCompany -> companyRepository.update(storedCompany, company))
                .orElse(null);
    }

    public void delete(Long id) {
        companyRepository.delete(id);
    }


    private Long generateNewId() {
        return getCompanies().stream()
                .mapToLong(Company::getId)
                .max()
                .orElse(0L) + 1;
    }
}
