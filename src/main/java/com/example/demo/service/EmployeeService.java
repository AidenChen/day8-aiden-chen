package com.example.demo.service;


import com.example.demo.exception.EmployeeCreatedException;
import com.example.demo.model.Employee;
import com.example.demo.repository.EmployeeRepository;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class EmployeeService {

    private final EmployeeRepository employeeRepository;
    public EmployeeService(EmployeeRepository employeeRepository) {
        this.employeeRepository = employeeRepository;
    }

    public Employee create(Employee employee) {
        employeeRepository.insert(employee);
        checkInsertEmployeeParams(employee);
        employee.setId(generateNewId());
        employee.setStatus(true);
        return employee;
    }

    public void checkInsertEmployeeParams(Employee employee) {
        if (!employee.isAgeValid()) {
            throw new EmployeeCreatedException("Employee must be 18~65 years old");
        }
        if (!employee.isSalaryValid()) {
            throw new EmployeeCreatedException("Employees over 30 years of age (inclusive) with a salary below 20000 cannot be created");
        }
    }

    public Employee update(Long id, Employee employee) {
        return employeeRepository.update(id, employee);
    }

    public Employee delete(Long id) {
        return employeeRepository.delete(id);
    }



    private Long generateNewId() {
        return employeeRepository.findAll().stream()
                .mapToLong(Employee::getId)
                .max()
                .orElse(0L) + 1;
    }

    public Employee findById(Long id) {
        return employeeRepository.findById(id);
    }

    public List<Employee> findAll() {
        return employeeRepository.findAll();
    }

    public List<Employee> findByGender(String gender) {
        return employeeRepository.findByGender(gender);
    }

    public List<Employee> findByPage(int pageNumber, int pageSize) {
        List<Employee> employees = employeeRepository.findAll();
        return employees.stream()
                .skip((long) (pageNumber - 1) * pageSize)
                .limit(pageSize)
                .collect(Collectors.toList());
    }
}
