package com.example.demo.exception;

public class EmployeeCreatedException extends RuntimeException{
    public EmployeeCreatedException(String message) {
        super(message);
    }
}
